# .profile

## Links

<table>
    <tr>
        <td><strong>Email</strong></td>
        <td><a href="mailto:bart@blender.org">bart@blender.org</a></td>
    </tr>
    <tr>
        <td><strong>Chat</strong></td>
        <td><a href="https://to.chat.blender.org/#/@bartvdbraak:blender.org">@bartvdbraak:blender.org</a></td>
    </tr>
    <tr>
        <td><strong>Mastodon</strong></td>
        <td><a rel="me" href="https://mstdn.social/@bartvdbraak">@bartvdbraak@mstdn.social</a></td>
    </tr>
    <tr>
        <td><strong>Website</strong></td>
        <td><a href="https://hellob.art">hellob.art</a></td>
    </tr>
    <tr>
        <td><strong>Reports</strong></td>
        <td><a href="https://projects.blender.org/bartvdbraak/.profile/src/branch/main/reports/2024.md">2024</a></td>
    </tr>
</table>

## About me

Hello, I'm Bart van der Braak, a DevOps Engineer at Blender. Originally from Amsterdam, I have spent most of my life in Zaandam. At Blender, I am dedicated to enhancing the provisioning and migration of services, as well as improving our CI/CD processes.

I hold a background in Data Science and my prior work experience has predominantly centered around Cloud Computing and DevOps.

I currently live with my wonderful girlfriend and our oversized orange cat.
